import pandas as pd
import random
import os

# Configuration
file_name = 'large_data.csv'
num_columns = 100
column_5_range = (1, 10)
other_columns_range = (1, 10)
target_file_size = 1024 * 1024 * 1024  # 1.5 GB
rows_per_chunk = 100000  # Number of rows per chunk

def generate_chunk(num_rows):
    data = {
        f"Column_{i+1}": [random.randint(other_columns_range[0], other_columns_range[1]) for _ in range(num_rows)]
        for i in range(num_columns)
    }
    data["Column_5"] = [random.randint(column_5_range[0], column_5_range[1]) for _ in range(num_rows)]
    return pd.DataFrame(data)

def main():
    # Create an empty DataFrame with the correct columns
    df = pd.DataFrame(columns=[f"Column_{i+1}" for i in range(num_columns)])
    df.to_csv(file_name, index=False)  # Write the header to the CSV file
    
    current_file_size = os.path.getsize(file_name)
    
    while current_file_size < target_file_size:
        chunk_df = generate_chunk(rows_per_chunk)
        chunk_df.to_csv(file_name, mode='a', header=False, index=False)
        current_file_size = os.path.getsize(file_name)
        print(f'Current file size: {current_file_size / (1024 * 1024):.2f} MB')

main()