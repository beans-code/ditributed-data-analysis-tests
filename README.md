# ditributed-data-analysis-tests



## Getting started

TODO description


## Installation
### 0 Prelude
This installation guide is purely for Ubuntu 22.04 LTS. Can be followed on ubuntu based distributions. If you are planning to use it on another distribution, you should fact-check every prompt and path because it's not the same.

### I Getting Java
Firstly, you should get stable java release, in my case it's java 11. To get java on your station open up a terminal and send a prompt. Afterwards, it will check your java version and change your directory to where java is:
    
    sudo apt install openjdk-11-jdk
    java -version
    cd /usr/lib/jvm
    
### II Configurating .bashrc file
Next step is to open .bashrc file: 

    sudo nano .bashrc

Then at the bottom of the file you should add this couple of prompts. Ofcourse, you need to change paths in following text for your own. After changes press CTRL + O then enter to save then CTRL + X to exit.

    export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
    export PATH=$PATH:/usr/lib/jvm/java-11-openjdk-amd64/bin
    export HADOOP_HOME=~/hadoop-3.4.0/
    export PATH=$PATH:$HADOOP_HOME/bin
    export PATH=$PATH:$HADOOP_HOME/sbin
    export HADOOP_MAPRED_HOME=$HADOOP_HOME
    export YARN_HOME=$HADOOP_HOME
    export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
    export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
    export HADOOP_OPTS="-Djava.library.path=$HADOOP_HOME/lib/native"
    export HADOOP_STREAMING=$HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-3.4.0.jar
    export HADOOP_LOG_DIR=$HADOOP_HOME/logs
    export PDSH_RCMD_TYPE=ssh

### III Installing ssh
Write in your terminal following prompt: 
    sudo apt-get install ssh

### IV Downloading and Configuring Hadoop
[Go to this site and download latest binary version of hadoop (3.4.0 for now)](https://hadoop.apache.org/releases.html "Hadoop website").<br>
Downloaded file move from **Downloads/ directory to ~/bin/**. 
Unpack hadoop using prompt: 

    tar -zxvf hadoop-3.4.0.tar.gz**

Further to confiure your hadoop write prompt:

    cd hadoop-3.4.0/etc/hadoop/use

Use ls to get sure you have *hadoop-env.sh*.
After getting into hadoop directory, we ought to rewrite following file using:

    sudo nano hadoop-env.sh

Then scroll down until you find: **#JAVA_HOME=/usr/java/testing hdfs dfs -ls**.<br> 
Just delete the **#** preceding command, and add your java path.<br>
It should look like this: **JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64**.<br>
After that just do like we did before CTRL + O then enter and CTRL + X.<br>
Now, we need to make changes in *core-sight.xml* for that just type:

    sudo nano core-sight.xml

Just add following text:

    <configuration>
     <property>
     <name>fs.defaultFS</name>
     <value>hdfs://localhost:9000</value>  </property>
     <property>
    <name>hadoop.proxyuser.dataflair.groups</name> <value>*</value>
     </property>
     <property>
    <name>hadoop.proxyuser.dataflair.hosts</name> <value>*</value>
     </property>
     <property>
    <name>hadoop.proxyuser.server.hosts</name> <value>*</value>
     </property>
     <property>
    <name>hadoop.proxyuser.server.groups</name> <value>*</value>
     </property>
    </configuration>

Following this, we ought to rewrite some more files. So we need to access another file by typing:

    sudo nano hdfs-site.xml

And like before, add following text:

    <configuration>
     <property>
     <name>dfs.replication</name>
     <value>1</value>
     </property>
    </configuration>

Next file we are rewriting is *mapred-site.xml*. Let's access:

    sudo nano mapred-site.xml

And then add text:

    <configuration>
     <property>
     <name>mapreduce.framework.name</name>  <value>yarn</value>
     </property>
     <property>
     <name>mapreduce.application.classpath</name>
    <value>$HADOOP_MAPRED_HOME/share/hadoop/mapreduce/*:$HADOOP_MAPRED_HOME/share/hadoop/mapreduce/lib/*</value>
     </property>
    </configuration>

Then we move to:

    sudo nano yarn-site.xml

Add text:

    <configuration>
     <property>
     <name>yarn.nodemanager.aux-services</name>
     <value>mapreduce_shuffle</value>
     </property>
     <property>
     <name>yarn.nodemanager.env-whitelist</name>
    <value>JAVA_HOME,HADOOP_COMMON_HOME,HADOOP_HDFS_HOME,HADOOP_CONF_DIR,CLASSPATH_PREP END_DISTCACHE,HADOOP_YARN_HOME,HADOOP_MAPRED_HOME</value>
     </property>
    </configuration>

After all that rewriting, we should connect to localhost by typing:

    ssh localhost

Then create ssh keys:

    ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
    cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
    chmod 0600 ~/.ssh/authorized_keys

Then format namenode and file system:

    hadoop-3.4.0/bin/hdfs namenode -format
    export PDSH_RCMD_TYPE=ssh

Now, we ready to start our node simply by writing command:

    start-all.sh

After that enter *http://localhost:9870/* in your web browser to check if hadoop is installed properly.

### V Installing Apache Pig
[Go to this site and download latest version of pig (pig-0.17.0 for now)](https://dlcdn.apache.org/pig/ "Pig website").
Move the downloaded *pig-0.17.0.tar.gz* to hadoop folder.
After that you need to extract this in thy folder: 

    tar zxvf pig-0.17.0.tar.gz

Following extraction you need to change *.bashrc* file once again, just add this and change paths for your own:

    export PATH=$PATH:/home/*your_username*/hadoop-3.4.0/pig-0.17.0/bin
    export PIG_HOME=/home/*your_username*/hadoop-3.4.0/pig-0.17.0
    export PIG_CLASSPATH=$HADOOP_HOME/conf

Procced to CTRL + O then enter and CTRL + X to close.
Next upadate the *.bashrc* using: 

    source .bashrc

After that try using. If the output with version appears you have successfully configured Pig.

    pig -version

### VI Installing Apache Spark
[Go to this site and download Apache Spark](https://spark.apache.org/downloads.html "Spark website").<br>
Unpack and move unpacked directory to /opt/spark:

    sudo mv spark-3.5.1-bin-hadoop3 /opt/spark

Like before update *.bashrc* file:

    nano ~/.bashrc

With following text:

    export SPARK_HOME=/opt/spark
    export PATH=$PATH:SPARK_HOME/bin

Then update *.bashrc*:

    source ~/.bashrc

To verify your installation have gone correctly, open terminal and type following command. If the installation was successful, you should see Spark starting up and end up with Spark logo and version information:

    spark-shell

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
